├───commons : modulos comunes de la app
│   ├───blocks
│   │   ├───environment : parametros de entorno
│   │   ├───exception : control de captura de exceptiones angular
│   │   ├───logger : control visual de mensajes en pantalla
│   │   └───router : modulo controlador de navegacion
│   ├───core : servicios comunes
│   └───layout : directivas que componen la plantilla de la app
├───incidencia : módulo abm
└───solicitud : módulo abm, configura sus propios estados en *.route.js

styles
-main.css : clases de estilo del layout
-aux-styles.css : clases personalizadas
-sidebar.css : clases específicas del menu
-table-responsive.css : clases que definen el comportamiento adaptable de las tablas, cada tabla debe tener sus columnas correspondientes definidas aquí

style="background-image: url('img/logo-fixed.png');"

gradle war -Ptest
gradle war -Pprod