package bo.com.ypfbandina.training.shared.rest.security.token;

import bo.com.ypfbandina.training.shared.rest.security.dto.JWTClaimSetDto;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Slf4j
@ApplicationScoped
public class TokenManager implements Serializable {

    @Inject
    RSAKeyManager rsaKeyManager;

    public String generateToken(JWTClaimSetDto claims) {
        RSAPrivateKey privateKey = (RSAPrivateKey) rsaKeyManager.getPrivateKey();
        JWSSigner signer = new RSASSASigner(privateKey);
        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload(claims.getJwtClaimsSet().toJSONObject()));
        String token = "";
        try {
            jwsObject.sign(signer);
            token = jwsObject.serialize();
            log.info("TokenManager generateToken() successful!!");
        } catch (Exception e) {
            log.info("TokenManager generateToken() error: " + e.getMessage());
        }

        return token;
    }

    /**
     * If token is valid, return JWTClaimSetDto
     */
    public JWTClaimSetDto verify(String token) {
        RSAPublicKey publicKey = (RSAPublicKey) rsaKeyManager.getPublicKey();
        JWSVerifier verifier = new RSASSAVerifier(publicKey);
        JWTClaimSetDto jwtClaimSetDto = null;
        try {
            JWSObject jwsObject = JWSObject.parse(token);
            if (jwsObject.verify(verifier)) {
                JSONObject jsonObjPayload = jwsObject.getPayload().toJSONObject();
                JWTClaimsSet jwtClaimSet = JWTClaimsSet.parse(jsonObjPayload);
                jwtClaimSetDto = new JWTClaimSetDto(jwtClaimSet);
                log.info("TokenManager verifyMask() successful!!");
            }
            ;
        } catch (Exception e) {
            log.info("TokenManager verifyMask() error: " + e.getMessage());
        }

        return jwtClaimSetDto == null ? new JWTClaimSetDto() : jwtClaimSetDto;
    }

}