package bo.com.ypfbandina.training.core.app.solicitud.service;

import bo.com.ypfbandina.training.core.app.solicitud.exception.SolicitudNotFoundException;
import bo.com.ypfbandina.training.core.app.solicitud.model.Solicitud;
import bo.com.ypfbandina.training.core.app.solicitud.repository.SolicitudRepository;
import bo.com.ypfbandina.training.shared.filter.PaginatedData;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.validation.Validator;
import java.util.Optional;

/**
 * Created by miguel on 6/23/16.
 */
@Slf4j
public class SolicitudService {

    @Inject
    public Validator validator;

    @Inject
    public SolicitudRepository solicitudRepository;

    public Solicitud findById(Long id) throws SolicitudNotFoundException {
        Optional<Solicitud> solicitudOpt = solicitudRepository.findById(id);
        if (!solicitudOpt.isPresent())
            throw new SolicitudNotFoundException();

        return solicitudOpt.get();
    }

    public PaginatedData<Solicitud> findByFilter(SolicitudFilter solicitudFilter) {
        return solicitudRepository.findByFilter(solicitudFilter);
    }
}
