package bo.com.ypfbandina.training.shared.rest.security.annotation;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by jorge on 25/02/2015.
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Qualifier
public @interface LoggedIn {

    public enum Access{
        ANONYMOUS,
        REGISTERED
    }

    @Nonbinding
    public Access[] permission() default {Access.REGISTERED};
}