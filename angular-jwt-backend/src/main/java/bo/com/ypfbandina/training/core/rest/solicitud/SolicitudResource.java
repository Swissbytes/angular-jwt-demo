package bo.com.ypfbandina.training.core.rest.solicitud;

import bo.com.ypfbandina.training.core.app.solicitud.model.Solicitud;
import bo.com.ypfbandina.training.core.app.solicitud.service.SolicitudFilter;
import bo.com.ypfbandina.training.core.app.solicitud.service.SolicitudService;
import bo.com.ypfbandina.training.shared.filter.PaginatedData;
import bo.com.ypfbandina.training.shared.json.JsonUtils;
import bo.com.ypfbandina.training.shared.json.JsonWriter;
import bo.com.ypfbandina.training.shared.rest.security.annotation.LoggedIn;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.util.HttpResponseCodes;
import org.joda.time.DateTime;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.text.SimpleDateFormat;

/**
 * Created by miguel on 10/19/16.
 */
@Slf4j
@Path("/solicitudes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class SolicitudResource {

    @Inject
    public SolicitudService solicitudService;

    @Inject
    public SolicitudJsonConverter solicitudJsonConverter;

    @Context
    public UriInfo uriInfo;
    
    @GET
    @LoggedIn
    @Path("/{type}/{userId}")
    public Response getCollaboratorsRequests(@PathParam("type") String type, @PathParam("userId") Long userId) {
        log.info(String.format("SolicitudResource.getCollaboratorsRequests received type: %s and userId: %s", type, userId));

//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//
//        JsonArray requests = new JsonArray();
//        JsonObject solicitud = new JsonObject();
//
//        solicitud.addProperty("code", "Sol1023");
//        solicitud.addProperty("date", sdf.format(DateTime.now().minusDays(17).toDate()));
//        solicitud.addProperty("user", "Freddy Quinteros Mamani");
//        solicitud.addProperty("position", "CONTRATISTA");
//
//        requests.add(solicitud);
//
//        solicitud.addProperty("code", "Sol999");
//        solicitud.addProperty("date", sdf.format(DateTime.now().minusDays(8).toDate()));
//        solicitud.addProperty("user", "Barry Allen Flash");
//        solicitud.addProperty("position", "CONTRATISTA");
//
//        requests.add(solicitud);
//
//        solicitud.addProperty("code", "Sol933");
//        solicitud.addProperty("date", sdf.format(DateTime.now().minusDays(2).toDate()));
//        solicitud.addProperty("user", "Marcos Quispe Paco");
//        solicitud.addProperty("position", "CONTRATISTA");
//
//        requests.add(solicitud);
//
//        return Response.ok().entity(new Gson().toJson(requests)).build();

        try {
            SolicitudFilter solicitudFilter = new SolicitudFilterExtractorFromUrl(uriInfo).getFilter();
            log.debug("Finding requests using filter: {}", solicitudFilter);

            PaginatedData<Solicitud> requests = solicitudService.findByFilter(solicitudFilter);

            log.debug("Found {} requests", requests.getNumberOfRows());

            JsonElement jsonWithPagingAndEntries = JsonUtils.getJsonElementWithPagingAndEntries(requests, solicitudJsonConverter);

            return Response.status(HttpResponseCodes.SC_OK).entity(JsonWriter.writeToString(jsonWithPagingAndEntries)).build();
        } catch (EJBTransactionRolledbackException iex) {
            log.warn("There was an issue when loading requests: ", iex);
            return Response.status(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @LoggedIn
    @Path("/{code}")
    public Response getRequest(@PathParam("code") String code) {
        log.info(String.format("SolicitudResource.getRequest received code: %s", code));

        JsonObject request = new JsonObject();
        request.addProperty("user", "Abel Perez Seras");
        request.addProperty("document", 4561856);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        JsonArray jsonArray = new JsonArray();
        JsonObject responseEntity = new JsonObject();

        responseEntity.addProperty("type", "ALTA");
        responseEntity.addProperty("resource", "Carpetas Compartidas");
        responseEntity.addProperty("description", "N:\\Certificaciones\\ - Escritura");
        responseEntity.addProperty("fromDate", sdf.format(DateTime.now().plusDays(5).toDate()));
        responseEntity.addProperty("toDate", sdf.format(DateTime.now().plusDays(10).toDate()));

        jsonArray.add(responseEntity);

        responseEntity.addProperty("type", "ALTA");
        responseEntity.addProperty("resource", "Gestión de Usuarios");
        responseEntity.addProperty("description", "Gerente");
        responseEntity.addProperty("fromDate", sdf.format(DateTime.now().plusDays(3).toDate()));
        responseEntity.addProperty("toDate", sdf.format(DateTime.now().plusDays(9).toDate()));

        jsonArray.add(responseEntity);

        responseEntity.addProperty("type", "BAJA");
        responseEntity.addProperty("resource", "Gestión de Usuarios");
        responseEntity.addProperty("description", "Inspector de NNPP");
        responseEntity.addProperty("department", "Talento Humano");

        jsonArray.add(responseEntity);

        responseEntity.addProperty("type", "BAJA");
        responseEntity.addProperty("resource", "Gestión de Usuarios");
        responseEntity.addProperty("description", "Inspector de Planilla");
        responseEntity.addProperty("department", "Talento Humano");

        jsonArray.add(responseEntity);

        request.add("requests", jsonArray);

        return Response.ok().entity(new Gson().toJson(request)).build();
    }

    @POST
    @LoggedIn
    public Response postResponse(String body) {
        log.debug("Adding a new user with body {}", body);

        if (body==null || body.isEmpty())
            return Response.noContent().build();

        final JsonObject jsonObject = new Gson().fromJson(body.trim(), JsonObject.class);

        String code = jsonObject.get("code").getAsString();
        Boolean response = jsonObject.get("response").getAsBoolean();
        String comments = jsonObject.get("comments").getAsString();

        log.debug(String.format("Solicitud with id: %s got response: %s with comments: %s", code, response.toString(), comments));

        return Response.ok().build();
    }
}
