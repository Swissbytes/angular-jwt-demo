package bo.com.ypfbandina.training.shared.json;

import bo.com.ypfbandina.training.shared.filter.PaginatedData;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by jorgeburgos on 2/1/16.
 */
public class JsonUtils {
    private JsonUtils() {
    }

    public static JsonElement getJsonElementWithId(Long id) {
        JsonObject idJson = new JsonObject();
        idJson.addProperty("id", id);

        return idJson;
    }

    public static <T> JsonElement getJsonElementWithPagingAndEntries(PaginatedData<T> paginatedData,
                                                                     EntityJsonConverter<T> entityJsonConverter)
    {
        JsonObject jsonWithEntriesAndPaging = new JsonObject();

        JsonObject jsonPaging = new JsonObject();
        jsonPaging.addProperty("totalRecords", paginatedData.getNumberOfRows());
        jsonPaging.addProperty("pagesCount", paginatedData.getPagesCount());

        jsonWithEntriesAndPaging.add("paging", jsonPaging);
        jsonWithEntriesAndPaging.add("entries", entityJsonConverter.convertToJsonElement(paginatedData.getRows()));

        return jsonWithEntriesAndPaging;
    }
}
