package bo.com.ypfbandina.training.shared.rest.security;

import bo.com.ypfbandina.training.core.app.jwt.service.JsonWebTokenService;
import bo.com.ypfbandina.training.shared.rest.security.annotation.LoggedIn;
import bo.com.ypfbandina.training.shared.rest.security.dto.AccountDto;
import bo.com.ypfbandina.training.shared.rest.security.dto.JWTClaimSetDto;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;
import org.jboss.resteasy.spi.metadata.ResourceMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;

@Slf4j
@Provider
public class SecurityInterceptor implements ContainerRequestFilter {

    private static final ServerResponse ACCESS_DENIED = new ServerResponse("{\"msg\": \"Access denied for this resource\"}",
            401, new Headers<Object>());
    private static final ServerResponse EXPIRED_SESSION = new ServerResponse("{\"msg\": \"Your session has expired\"}",
            440, new Headers<Object>());

    @Inject
    JsonWebTokenService jsonWebTokenService;

    @Context
    HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker)
                requestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();


        if (method.isAnnotationPresent(LoggedIn.class)) {
            log.debug("--- Method with annotation LoggedIn");

            JWTClaimSetDto jwtClaimSetDto = jsonWebTokenService.extractJWTClaimSetDto(servletRequest);
            AccountDto accountDto = jwtClaimSetDto.getAccount();

            if (accountDto == null)
                requestContext.abortWith(ACCESS_DENIED);

            if (jwtClaimSetDto.isExpired())
                requestContext.abortWith(EXPIRED_SESSION);

        } else
            log.debug("--- Method without annotation LoggedIn");
    }
}