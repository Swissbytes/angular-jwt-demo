package bo.com.ypfbandina.training.core.app.solicitud.service;

import bo.com.ypfbandina.training.shared.filter.GenericFilter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by miguel on 23/6/16.
 */
@Getter
@Setter
@ToString
public class SolicitudFilter extends GenericFilter {
    private String user;

    public SolicitudFilter() {
    }

    public SolicitudFilter(String user) {
        this.user = user;
    }
}
