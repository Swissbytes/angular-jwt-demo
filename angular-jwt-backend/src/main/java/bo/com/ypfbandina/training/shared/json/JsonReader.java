package bo.com.ypfbandina.training.shared.json;

import bo.com.ypfbandina.training.shared.json.exception.InvalidJsonException;
import com.google.gson.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jorgeburgos on 2/1/16.
 */
public class JsonReader {
    public static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static JsonObject readAsJsonObject(String json) {
        return readJsonAs(json, JsonObject.class);
    }

    private static <T> T readJsonAs(String json, Class<T> jsonClass) {
        if (json == null || json.trim().isEmpty()) {
            throw new InvalidJsonException("Json String can not be null");
        }

        try {
            return new Gson().fromJson(json, jsonClass);
        } catch (JsonSyntaxException e) {
            throw new InvalidJsonException(e);
        }
    }

    public static Date getDateOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        String formattedDate = property.getAsString();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        try {
            return format.parse(formattedDate);
        } catch (ParseException e) {
            throw new InvalidJsonException(e);
        }
    }

    public static String getStringOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }
        
        return property.getAsString();
    }

    public static Integer getIntegerOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsInt();
    }

    public static Byte getByteOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsByte();
    }

    public static Short getShortOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsShort();
    }

    public static Long getLongOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsLong();
    }

    public static Boolean getBooleanOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsBoolean();
    }

    public static Double getDoubleOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsDouble();
    }

    public static BigDecimal getBigDecimalOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsBigDecimal();
    }

    public static JsonArray getJsonArrayOrNull(JsonObject jsonObject, String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsJsonArray();
    }

    private static boolean isJsonElementNull(JsonElement element) {
        return element == null || element.isJsonNull();
    }
}
