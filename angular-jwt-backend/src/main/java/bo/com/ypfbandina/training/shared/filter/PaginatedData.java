package bo.com.ypfbandina.training.shared.filter;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jorgeburgos on 1/28/16.
 */
@Getter
@Setter
public class PaginatedData<T> {
    private int numberOfRows;
    private int pagesCount;
    private List<T> rows;

    public PaginatedData(int numOfRows, List<T> rows) {
        this.numberOfRows = numOfRows;
        this.rows = rows;
    }

    public PaginatedData(int numOfRows, List<T> rows, int pagesCount) {
        this.numberOfRows = numOfRows;
        this.rows = rows;
        this.pagesCount = pagesCount;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public List<T> getRows() {
        return rows;
    }

    public T getRow(int index) {
        if (index >= rows.size()) {
            return null;
        }

        return rows.get(index);
    }

    @Override
    public String toString() {
        return "PaginatedData{" +
                " numberOfRows=" + numberOfRows +
                ", rows=" + rows +
                ", pagesCount=" + pagesCount +
                '}';
    }
}
