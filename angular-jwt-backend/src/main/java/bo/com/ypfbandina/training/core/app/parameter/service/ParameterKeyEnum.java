package bo.com.ypfbandina.training.core.app.parameter.service;

import lombok.Getter;

public enum ParameterKeyEnum {
    PATH_PRIVATE_KEY_FILE("path.private.key.file"),
    PATH_PUBLIC_KEY_FILE("path.public.key.file");

    @Getter
    private String key;

    ParameterKeyEnum(String key) {
        this.key = key;
    }
}
