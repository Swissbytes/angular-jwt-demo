package bo.com.ypfbandina.training.core.app.solicitud.repository;

import bo.com.ypfbandina.training.core.app.solicitud.model.Solicitud;
import bo.com.ypfbandina.training.core.app.solicitud.service.SolicitudFilter;
import bo.com.ypfbandina.training.shared.filter.PaginatedData;
import bo.com.ypfbandina.training.shared.persistence.Repository;

import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Stateless
public class SolicitudRepository extends Repository {
    @Override
    protected Class getPersistentClass() {
        return Solicitud.class;
    }

    public Optional<Solicitud> findById(Long id) {
        Optional<Solicitud> roleOptional = findById(Solicitud.class, id);
        return roleOptional;
    }

    public PaginatedData<Solicitud> findByFilter(SolicitudFilter roleFilter) {
        StringBuilder clause = new StringBuilder("WHERE e.id IS NOT NULL");

        Map<String, Object> queryParameters = new HashMap<>();

        if (roleFilter.getUser() != null) {
            clause.append(" AND UPPER(e.user) LIKE UPPER(:user)");
            queryParameters.put("user", "%" + roleFilter.getUser() + "%");
        }

        return findByParameters(clause.toString(), roleFilter.getPaginationData(), queryParameters, "code ASC");
    }
}