package bo.com.ypfbandina.training.core.app.jwt.service;

import bo.com.ypfbandina.training.shared.rest.security.dto.AccountDto;
import bo.com.ypfbandina.training.shared.rest.security.dto.JWTClaimSetDto;
import bo.com.ypfbandina.training.shared.rest.security.token.TokenManager;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Slf4j
public class JsonWebTokenService {
    private static final String AUTHORIZATION_PROPERTY = "Authorization";

    @Inject
    TokenManager tokenManager;

    public String generateAuthenticationToken(AccountDto accountDto) {
        String token = "";
        if (accountDto != null) {
            token = tokenManager.generateToken(new JWTClaimSetDto(accountDto));
        }
        log.info("JsonWebTokenService generateAuthenticationToken() token: " + token);
        return token;
    }

    public JWTClaimSetDto verifyTokenAndGetJWTClaimSetDto(String token) {
        return tokenManager.verify(token);
    }

    public Optional<AccountDto> verifyTokenAndGetAccountDto(String token) {
        JWTClaimSetDto jwtClaimSetDto = tokenManager.verify(token);

        return (jwtClaimSetDto != null && jwtClaimSetDto.getAccount() != null)
                ? Optional.of(jwtClaimSetDto.getAccount()) : Optional.<AccountDto>empty();
    }

    public JWTClaimSetDto extractJWTClaimSetDto(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_PROPERTY);
        if (token != null && !token.isEmpty()) {
            return verifyTokenAndGetJWTClaimSetDto(token);
        }
        return new JWTClaimSetDto();
    }

    public Optional<AccountDto> extractAccountDtoOptional(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_PROPERTY);
        if (token != null && !token.isEmpty()) {
            return verifyTokenAndGetAccountDto(token);
        }
        return Optional.<AccountDto>empty();
    }
}