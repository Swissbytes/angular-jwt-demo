package bo.com.ypfbandina.training.shared.rest.security.dto;

import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;
import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class JWTClaimSetDto {

    private JWTClaimsSet jwtClaimsSet;
    private AccountDto accountDto;

    private void init() {
        jwtClaimsSet.setIssuer("web");
        jwtClaimsSet.setExpirationTime(new DateTime(new Date()).plusYears(1).toDate()); //1 year
        jwtClaimsSet.setNotBeforeTime(new Date());
        jwtClaimsSet.setIssueTime(new Date());
        jwtClaimsSet.setJWTID(UUID.randomUUID().toString());
    }

    public JWTClaimSetDto() {
        jwtClaimsSet = new JWTClaimsSet();
        init();
        jwtClaimsSet.setCustomClaim("account", null);
    }

    public JWTClaimSetDto(AccountDto account) {
        jwtClaimsSet = new JWTClaimsSet();
        init();
        jwtClaimsSet.setCustomClaim("account", account.getJsonObject());
    }

    public JWTClaimSetDto(JWTClaimsSet jwtClaimsSet) {
        this.jwtClaimsSet = jwtClaimsSet;
    }


    public JWTClaimsSet getJwtClaimsSet() {
        return jwtClaimsSet;
    }


    public boolean isExpired() {
        return Calendar.getInstance().getTime().after(jwtClaimsSet.getExpirationTime());
    }

    public AccountDto getAccount() {
        if (accountDto == null && jwtClaimsSet.getCustomClaim("account") != null) {
            JSONObject accountJson = (JSONObject)jwtClaimsSet.getCustomClaim("account");
            accountDto = new AccountDto(accountJson);
        }
        return accountDto;
    }

    public void setAccount(AccountDto account) {
        jwtClaimsSet.setCustomClaim("account", account);
    }
}
