package bo.com.ypfbandina.training.shared.filter;

/**
 * Created by jorgeburgos on 1/28/16.
 */
public class PaginationData {
    private int firstResult;
    private int maxResults;
    private String orderField;
    private OrderMode orderMode;

    public PaginationData(int firstResult, int maxResults, String orderField, OrderMode orderMode) {
        this.firstResult = firstResult;
        this.maxResults = maxResults;
        this.orderField = orderField;
        this.orderMode = orderMode;
    }

    public int getFirstResult() {
        return firstResult;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public String getOrderField() {
        return orderField;
    }

    public OrderMode getOrderMode() {
        return orderMode;
    }

    public boolean isAscending() {
        return orderMode == OrderMode.ASCENDING;
    }

    @Override
    public String toString() {
        return "PaginationData{" +
                "firstResult=" + firstResult +
                ", maxResults=" + maxResults +
                ", orderField='" + orderField + '\'' +
                ", orderMode=" + orderMode +
                '}';
    }

    public enum OrderMode {
        ASCENDING, DESCENDING
    }
}
