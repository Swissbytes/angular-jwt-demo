package bo.com.ypfbandina.training.core.app.solicitud.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "SOLICITUDES")
@Getter
@Setter
@ToString
public class Solicitud {
    @Id
    @Column(name = "PK_SOLICITUDES_ID")
    private Long id;

    @NotNull
    @Column(name = "CODE")
    private String code;

    @NotNull
    @Column(name = "DATE")
    private Date date;

    @NotNull
    @Column(name = "USER")
    private String user;

    @NotNull
    @Column(name = "POSITION")
    private String position;
}