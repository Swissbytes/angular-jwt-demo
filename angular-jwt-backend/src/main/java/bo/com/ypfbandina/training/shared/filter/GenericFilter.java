package bo.com.ypfbandina.training.shared.filter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by jorgeburgos on 1/28/16.
 */
public class GenericFilter {
    private PaginationData paginationData;

    public GenericFilter() {
    }

    public GenericFilter(PaginationData paginationData) {
        this.paginationData = paginationData;
    }

    public PaginationData getPaginationData() {
        return paginationData;
    }

    public void setPaginationData(PaginationData paginationData) {
        this.paginationData = paginationData;
    }

    public boolean hasPaginationData() {
        return getPaginationData() != null;
    }

    public boolean hasOrderField() {
        return hasPaginationData() && getPaginationData().getOrderField() != null;
    }


    protected String getAttributes(Class _class, Object obj) {
        Field[] userFields = _class.getDeclaredFields();
        return Arrays.asList(userFields).stream()
                .map(i -> getFieldDetail(i, obj))
                .collect(Collectors.joining(",", _class.getSimpleName() + "[", "]"));
    }

    private String getFieldDetail(Field field, Object obj) {
        try {
            field.setAccessible(true);
            return " " + field.getName() + "=" + field.get(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String toString() {
        return getAttributes(this.getClass(), this);
    }

}
