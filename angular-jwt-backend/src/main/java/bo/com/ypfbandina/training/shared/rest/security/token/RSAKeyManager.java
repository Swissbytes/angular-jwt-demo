package bo.com.ypfbandina.training.shared.rest.security.token;

import bo.com.ypfbandina.training.core.app.parameter.service.ParameterKeyEnum;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.io.*;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * It manages private and public keys.
 * <p>
 * If keys are not generated yet, it will create and persist them.
 * Each key has its own file.
 * <p>
 * Created on 25/02/2015.
 *
 * @author Jorge Burgos L.
 */
@Slf4j
@ApplicationScoped
public class RSAKeyManager implements Serializable {

    /**
     * String to hold name of the encryption algorithm.
     */
    private final String ALGORITHM = "RSA";

    /**
     * String to hold name of private key file.
     */
    private String PRIVATE_KEY_FILE = "";

    /**
     * String to hold name of public key file.
     */
    private String PUBLIC_KEY_FILE = "";


    private PublicKey publicKey;

    private PrivateKey privateKey;

    /**
     * Generate key which contains a pair of private and public key using 1024
     * bytes. Store the set of keys in Private.key and Public.key files.
     * @throws java.security.NoSuchAlgorithmException
     * @throws IOException
     * @throws FileNotFoundException
     */
    public void generateKey() {
        try {
            final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
            keyGen.initialize(1024);
            final KeyPair key = keyGen.generateKeyPair();

            File privateKeyFile = new File(PRIVATE_KEY_FILE);
            File publicKeyFile = new File(PUBLIC_KEY_FILE);

            // Create files to store public and private key
            if (privateKeyFile.getParentFile() != null) {
                privateKeyFile.getParentFile().mkdirs();
            }

            privateKeyFile.createNewFile();

            if (publicKeyFile.getParentFile() != null) {
                publicKeyFile.getParentFile().mkdirs();
            }

            publicKeyFile.createNewFile();

            // Saving the Public key in a file
            ObjectOutputStream publicKeyOS = new ObjectOutputStream(new FileOutputStream(publicKeyFile));
            publicKeyOS.writeObject(key.getPublic());
            publicKeyOS.close();

            // Saving the Private key in a file
            ObjectOutputStream privateKeyOS = new ObjectOutputStream(new FileOutputStream(privateKeyFile));
            privateKeyOS.writeObject(key.getPrivate());
            privateKeyOS.close();
            log.info("generateKey() successful!!");
        } catch (Exception e) {
            log.info("generateKey() error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * The method checks if the pair of public and private key has been generated.
     * @return flag indicating if the pair of keys were generated.
     */
    private boolean areKeysPresent() {

        File privateKey = new File(PRIVATE_KEY_FILE);
        File publicKey = new File(PUBLIC_KEY_FILE);

        return privateKey.exists() && publicKey.exists();
    }

    private void loadKeys() throws Exception {
        if (!areKeysPresent()) {
            generateKey();
        }

        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_FILE));
        publicKey = (PublicKey) inputStream.readObject();

        inputStream = new ObjectInputStream(new FileInputStream(PRIVATE_KEY_FILE));
        privateKey = (PrivateKey) inputStream.readObject();
    }

    @PostConstruct
    public void initialize() throws Exception {
        PUBLIC_KEY_FILE = System.getProperty(ParameterKeyEnum.PATH_PUBLIC_KEY_FILE.getKey());
        PRIVATE_KEY_FILE = System.getProperty(ParameterKeyEnum.PATH_PRIVATE_KEY_FILE.getKey());

        log.info("initialize RSAKeyManager");
        loadKeys();
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

}
