package bo.com.ypfbandina.training.core.app.solicitud.exception;

import javax.ejb.ApplicationException;

/**
 * Created by miguel on 6/23/16.
 */
@ApplicationException
public class SolicitudNotFoundException extends RuntimeException {
}
