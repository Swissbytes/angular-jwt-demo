package bo.com.ypfbandina.training.core.rest.user;

import bo.com.ypfbandina.training.core.app.jwt.service.JsonWebTokenService;
import bo.com.ypfbandina.training.shared.rest.security.dto.AccountDto;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by miguel on 10/14/16.
 */
@Slf4j
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {

    @Inject
    public JsonWebTokenService jWTService;

    @POST
    @Path("/login/{user}/{password}")
    public Response login(@PathParam("user") String user, @PathParam("password") String password) {
        log.info(String.format("UserResource.login received user: %s, password: %s", user, password));

        AccountDto accountDto = new AccountDto();
        accountDto.setFirstName("Miguel");
        accountDto.setLastName("Ordoñez");
        accountDto.setEmail("miguel.ordonez@swissbytes.ch");
        accountDto.setId(100L);


        String tokenAuthorization = jWTService.generateAuthenticationToken(accountDto);

        JsonObject responseEntity = new JsonObject();
        responseEntity.addProperty("user", user);
        responseEntity.addProperty("authorization", tokenAuthorization);

        if (password.equals("123456"))
            return Response.ok().entity(new Gson().toJson(responseEntity)).build();
        else
            return Response.noContent().build();
    }
}
