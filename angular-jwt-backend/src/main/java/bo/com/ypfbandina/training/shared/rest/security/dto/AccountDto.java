package bo.com.ypfbandina.training.shared.rest.security.dto;

import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

public class AccountDto {

    JSONObject jsonObject;

    public AccountDto() {this.jsonObject = new JSONObject();}

    public AccountDto(JSONObject jsonObject) {this.jsonObject = jsonObject;}

    public AccountDto(String json) {
        this.jsonObject = (JSONObject) JSONValue.parse(json);
    }

    public boolean isEmpty() {
        return jsonObject.isEmpty();
    }

    public String toString() {
        return jsonObject.toString();
    }

    public String toJSONString() {
        return jsonObject.toJSONString();
    }

    public Long getId() {
        return (Long)jsonObject.get("id");
    }

    public void setId(Long id) {
        jsonObject.put("id", id);
    }

    public String getEmail() {
        return (String)jsonObject.get("email");
    }

    public void setEmail(String email) {
        setField("email", email);
    }

    public String getFirstName() {
        return getField("firstName");
    }

    public void setFirstName(String name) {
        setField("firstName", name);
    }

    public String getLastName() {
        return getField("lastName");
    }

    public void setLastName(String lastName) {
        setField("lastName", lastName);
    }

    private void setField(String name, String value) {
        jsonObject.put(name, value);
    }

    private String getField(String name) {
        return (String)jsonObject.get(name);
    }

    public void addAttribute(String name, String value) {
        jsonObject.put(name, value);
        JWTClaimsSet jwtClaimsSet = new JWTClaimsSet();
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

//    public static AccountDto mapFrom(User user) {
//        AccountDto accountDto = new AccountDto();
//        accountDto.setId(user.getId());
//        accountDto.setFirstName(user.getFirstName());
//        accountDto.setLastName(user.getLastName());
//        accountDto.setEmail(user.getEmail());
//        return accountDto;
//    }
}
