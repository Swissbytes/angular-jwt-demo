package bo.com.ypfbandina.training.core.rest.solicitud;

import bo.com.ypfbandina.training.core.app.solicitud.model.Solicitud;
import bo.com.ypfbandina.training.shared.json.EntityJsonConverter;
import bo.com.ypfbandina.training.shared.json.JsonReader;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.enterprise.context.ApplicationScoped;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by miguel on 23/6/16.
 */
@ApplicationScoped
public class SolicitudJsonConverter implements EntityJsonConverter<Solicitud> {


    @Override
    public Solicitud convertFrom(String json) {
        SimpleDateFormat format = new SimpleDateFormat(JsonReader.DATE_FORMAT);
        final JsonObject jsonObject = JsonReader.readAsJsonObject(json);

        final Solicitud solicitud = new Solicitud();
        solicitud.setId(JsonReader.getLongOrNull(jsonObject, "id"));
        solicitud.setCode(JsonReader.getStringOrNull(jsonObject, "code"));
        try {
            solicitud.setDate(format.parse(JsonReader.getStringOrNull(jsonObject, "date")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        solicitud.setUser(JsonReader.getStringOrNull(jsonObject, "user"));
        solicitud.setPosition(JsonReader.getStringOrNull(jsonObject, "position"));

        return solicitud;
    }

    @Override
    public JsonElement convertToJsonElement(Solicitud solicitud) {
        SimpleDateFormat format = new SimpleDateFormat(JsonReader.DATE_FORMAT);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", solicitud.getId());
        jsonObject.addProperty("code", solicitud.getCode());
        jsonObject.addProperty("date", format.format(solicitud.getDate()));
        jsonObject.addProperty("user", solicitud.getUser());
        jsonObject.addProperty("position", solicitud.getPosition());

        return jsonObject;
    }
}
