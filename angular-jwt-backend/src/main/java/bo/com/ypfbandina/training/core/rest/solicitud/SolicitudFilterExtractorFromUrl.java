package bo.com.ypfbandina.training.core.rest.solicitud;

import bo.com.ypfbandina.training.core.app.solicitud.service.SolicitudFilter;
import bo.com.ypfbandina.training.shared.filter.AbstractFilterExtractorFromUrl;

import javax.ws.rs.core.UriInfo;

/**
 * Created by jorgeburgos on 2/1/16.
 */
public class SolicitudFilterExtractorFromUrl extends AbstractFilterExtractorFromUrl {

    public SolicitudFilterExtractorFromUrl(UriInfo uriInfo) {
        super(uriInfo);
    }

    public SolicitudFilter getFilter() {
        SolicitudFilter solicitudFilter = new SolicitudFilter();
        solicitudFilter.setPaginationData(extractPaginationData());
        solicitudFilter.setUser(getUriInfo().getQueryParameters().getFirst("user"));

        return solicitudFilter;
    }

    @Override
    protected String getDefaultSortField() {
        return "code";
    }
}
