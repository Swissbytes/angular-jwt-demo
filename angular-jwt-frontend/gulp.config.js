/**
 * Created by jorgeburgos on 12/2/15.
 */

module.exports = function () {
    var webapp = './src/main/webapp/';
    var webappTemp = webapp + '.temp/';
    var temp = './build/tmp/';
    var app = webapp + 'app/';
    var webappIgnore = '/src/main/webapp/';
    var environmentPath = app + 'commons/blocks/environment/';
    var optimizedDest = webapp + '.optimized/';
    var report = webappTemp + 'report/';
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({devDependencies: true})['js'];
    var webappTest = webapp + 'tests/';

    var integrationTest = webappTest + 'server-integration/';

    var config = {
        temp: temp,
        optimizedDest: optimizedDest,
        app: app,
        webContext: '',
        webapp: webapp,
        webappTemp: webappTemp,
        webappIgnore: webappIgnore,
        index: webapp + 'index.html',
        indexTemplate: webapp + 'index.template.html',
        htmltemplates: app + '**/*.html',
        report: report,

        images: {
            src: webapp + 'img/**/*.*',
            dest: optimizedDest + 'img/'

        },

        fonts: {
            src: [
                webapp + '.bower_components/components-font-awesome/fonts/*.*',
                webapp + 'fonts/*.*'
            ],
            dest: optimizedDest + 'fonts/'
        },

        allSrcJs: [
            webapp + 'app/**/*.js',
            './*.js'
        ],

        js: [
            app + '**/*.module.js',
            app + '**/*.js',
            '!' + app + '**/*.spec.js'
        ],

        styles: {
            less: {
                mainFile: webapp + 'styles/less/main.less',
                filenameCompressed: 'less.min.css',
                destDir: webappTemp,
                path: webappTemp + 'less.min.css'
            },

            css: {
                src: [
                    webapp + 'styles/css/**/*.*',
                    webappTemp + 'less.min.css'
                ]
            }
        },


        /**
         * Karma and testing settings
         */

        specHelpers: [webappTest + 'test-helpers/*.js'],
        serverIntegrationSpecs: [integrationTest + '**/*.spec.js'],

        /**
         * Bower and NPM locations
         */
        bower: {
            json: require('./bower.json'),
            directory: 'src/main/webapp/.bower_components',
            ignorePath: '../../..'

        },

        /**
         * Template cache
         */
        templateCache: {
            file: 'template.js',
            options: {
                module: 'app.core',
                standAlone: false,
                root: 'app/'
            }
        },


        /**
         * Environment
         */
        env: {
            directory: environmentPath,
            module: 'blocks.environment',
            filename: 'environment.js',

            localJson: environmentPath + "local.json",
            devJson: environmentPath + "dev.json",
            testJson: environmentPath + "test.json",
            prodJson: environmentPath + "prod.json"
        }
    };

    config.getWiredepDefaultOptions = function () {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath,
            fileTypes: {
                html: {
                    replace: {
                        js: '<script src="{{filePath}}"></script>'
                    }
                }
            }
        };

        return options;
    };

    config.karma = getkarmaOptions();


    return config;

    ////////////////

    function getkarmaOptions() {
        var options = {
            files: [].concat(
                bowerFiles,
                config.specHelpers,
                app + '**/*.module.js',
                app + '**/*.js',
                config.webappTemp + 'template.js'
            ),

            exclude: [],
            coverage: {
                dir: report + 'coverage',
                reporters: [
                    {type: 'html', subdir: 'report-html'},
                    {type: 'locv', subdir: 'report-lcov'},
                    {type: 'text-summary'}
                ]
            },

            preprocessors: {}
        };

        options.preprocessors[webapp + '**/!(*.spec)+(.js)'] = ['coverage'];

        return options;
    }

};
