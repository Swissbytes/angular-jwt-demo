/**
 * Created by jorgeburgos on 12/2/15.
 */

(function () {
    'use strict';

    angular
        .module('app', [
            /* Shared modules */
            'app.core',
            'ngCookies',
            'ui.bootstrap',

            /* Feature areas */
            'app.layout',
            'app.solicitud'
        ]);
})();