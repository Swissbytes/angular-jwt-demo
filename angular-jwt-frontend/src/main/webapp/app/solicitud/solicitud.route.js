
(function () {
    'use strict';

    angular
        .module('app.solicitud')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'solicitud',
                config: {
                    abstract: true,
                    template: '<div ui-view></div>',
                    url: '/solicitud'
                }
            },
            {
                state: 'solicitud.list',
                config: {
                    abstract: true,
                    template: '<div ui-view></div>',
                    url: '/list'
                }
            },
            {
                state: 'solicitud.list.collaborators',
                config: {
                    url: '/collaborators',
                    templateUrl: 'app/solicitud/solicitud-list.html',
                    controller: 'SolicitudListController',
                    controllerAs: 'vm',
                    extras: {
                        navId: 10,
                        type: 'collaborators',
                        title: 'Solicitudes de Mis Colaboradores',
                        breadcrumbs: ['Solicitudes', 'Mis Colaboradores']
                    }
                }
            },
            {
                state: 'solicitud.list.mymanagement',
                config: {
                    url: '/mymanagement',
                    templateUrl: 'app/solicitud/solicitud-list.html',
                    controller: 'SolicitudListController',
                    controllerAs: 'vm',
                    extras: {
                        navId: 11,
                        type: 'mymanagement',
                        title: 'Solicitudes de Mi Gerencia',
                        breadcrumbs: ['Solicitudes', 'Mi Gerencia']
                    }
                }
            },
            {
                state: 'solicitud.list.othermanagement',
                config: {
                    url: '/othermanagement',
                    templateUrl: 'app/solicitud/solicitud-list.html',
                    controller: 'SolicitudListController',
                    controllerAs: 'vm',
                    extras: {
                        navId: 12,
                        type: 'othermanagement',
                        title: 'Solicitudes de Otra Gerencia',
                        breadcrumbs: ['Solicitudes', 'Otra Gerencia']
                    }
                }
            },
            {
                state: 'solicitud.detail',
                config: {
                    url: '/detail/:type/:solicitudId',
                    templateUrl: 'app/solicitud/solicitud-detail.html',
                    controller: 'SolicitudDetailController',
                    controllerAs: 'vm',
                    extras: {
                        navId: 13,
                        title: 'Solicitud de Aprobación',
                        breadcrumbs: ['Solicitudes', 'Detalle']
                    }
                }
            }
        ];
    }

})();