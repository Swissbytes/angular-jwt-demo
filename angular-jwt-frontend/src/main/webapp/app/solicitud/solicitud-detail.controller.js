/**
 * Created by miguel.ordonez on 22/08/2016.
 */
(function () {'use strict';

    angular
        .module('app.solicitud')
        .controller('SolicitudDetailController', SolicitudDetailController);

    SolicitudDetailController.$inject = ['$scope', '$stateParams', '$state', 'dataservice', 'logger', '$uibModal'];

    function SolicitudDetailController($scope, $stateParams, $state, dataservice, logger, $uibModal) {
        var vm = this;

        vm.solicitud = {};
        vm.comment = '';
        vm.gotosolicitudes = gotosolicitudes;
        vm.approve = approve;
        vm.reject = reject;

        activate();

        function gotosolicitudes() {
            $state.go('solicitud.list.' + $stateParams.type);
        }

        function activate() {
            $scope.$watch(dataservice.isUserLoaded, function (result) {
                if (result) {
                    update().then(function () {
                        //logger.info("Vista activada");
                    });
                }
            });
        }

        function update() {
            return dataservice.getSolicitud($stateParams.solicitudId).then(function(response) {
                vm.solicitud = response.data;
            });
        }

        function approve() {

            var data = {
                'code' : $stateParams.solicitudId,
                'comments' : '',
                'response' : 'true'
            };

            dataservice.postSolicitudResponse(data).then(function() {
                logger.success('Solicitud Aprobada con POST');
                gotosolicitudes();
            });
        }

        function reject() {
            raiseConfirmationModal('Rechazar Solicitud ' + $stateParams.solicitudId, 'Por favor, ingrese una explicación:')
        }

        function raiseConfirmationModal(title, message) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/commons/layout/confirmation-modal.html',
                controller: 'ConfirmationModalController',
                controllerAs: 'pm',
                resolve: {
                    properties: function() {
                        return {
                            title: title,
                            okLabel: 'Rechazar',
                            cancelLabel: 'Cancelar',
                            message: message
                        };
                    }
                }
            });

            modalInstance.result.then(
                function (comment) {
                    //ok
                    //logger.info(comment);

                    var data = {
                        'code' : $stateParams.solicitudId,
                        'comments' : comment,
                        'response' : 'false'
                    };

                    dataservice.postSolicitudResponse(data).then(function() {
                        logger.success('Solicitud Rechazada con POST');
                        gotosolicitudes();
                    });

                },
                function () {
                    //cancel
                    logger.info('No se realizó ninguna acción');
                }
            );
        }
    }

})();
