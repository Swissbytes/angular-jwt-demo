/**
 * Created by miguel on 7/6/16.
 */
(function () {'use strict';

    angular
        .module('app.solicitud')
        .controller('SolicitudListController', SolicitudListController);

    SolicitudListController.$inject = ['$scope', '$state', 'dataservice', 'logger'];
    /* @ngInject */
    /**
     * @return {string}
     * @return {string}
     */
    function SolicitudListController($scope, $state, dataservice, logger) {
        var vm = this;

        vm.solicitudes = {};

        vm.gotoDetail = gotoDetail;

        vm.code = '';
        vm.orderby = 'code';
        vm.order = '%2B';

        vm.paging = {};
        vm.paging.left = [];
        vm.paging.right = [];
        vm.paging.first_prev = false;
        vm.paging.next_last = true;
        vm.paging.current = 0;
        vm.paging.size = 10;

        vm.sorticon = sorticon;
        vm.toggleorder = toggleorder;
        vm.pagTo = pagTo;

        function update() {
            var paramCode = "code=" + vm.code;
            var paramOrderby = "sort=" + vm.order + vm.orderby;
            var paging = "page=" +vm.paging.current+"&per_page="+vm.paging.size;

            return dataservice.getSolicitudes($state.current.extras.type, dataservice.getAccount().id, paramCode,paramOrderby,paging).then(function(response) {
                vm.solicitudes = response.data.entries;

                vm.paging.total = response.data.paging.pagesCount;
                configPagControl();
            });
        }

        activate();

        function activate() {
            $scope.$watch(dataservice.isUserLoaded, function (result) {
                if (result) {
                    update().then(function () {
                        //logger.info("Vista activada");
                    });
                }
            });
        }

        function gotoDetail(solicitud) {
            $state.go('solicitud.detail', {solicitudId: solicitud.code, type: $state.current.extras.type});
        }

        /////PAGINATION

        function toggleorder(fieldBy) {
            vm.order = (vm.order === "%2B") ? "%2D" : "%2B";
            vm.orderby = fieldBy;
            vm.paging.current = 0;
            update();
        }

        function sorticon(sortfield) {
            var iconname = "fa fa-sort";

            if (sortfield == vm.orderby) {
                iconname = (vm.order === "%2B") ? "fa fa-sort-asc" : "fa fa-sort-desc";
            }

            return iconname;
        }

        function pagTo(pg){
            vm.paging.current = pg;
            update();
        }

        function configPagControl(){
            vm.paging.left=[];
            vm.paging.right=[];

            if( vm.paging.current >1 ){ vm.paging.left.push( vm.paging.current-2); }
            if( vm.paging.current >0 ){ vm.paging.left.push( vm.paging.current-1); }
            if( vm.paging.current < vm.paging.total-1){ vm.paging.right.push( vm.paging.current+1); }
            if( vm.paging.current < vm.paging.total-2){ vm.paging.right.push( vm.paging.current+2); }

            vm.paging.first_prev = (vm.paging.current !== 0);
            vm.paging.next_last = (vm.paging.current !== vm.paging.total-1);
        }


    }
})();
