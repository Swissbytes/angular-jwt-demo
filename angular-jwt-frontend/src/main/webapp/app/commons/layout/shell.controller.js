/**
 * Created by jorgeburgos on 12/2/15.
 */

(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('Shell', Shell);

    Shell.$inject = ['config'];

    function Shell(config) {
        var vm = this;

        vm.title = config.appTitle;
        vm.isBusy = true;
        
        activate();

        function activate() {
        }
    }

})();
