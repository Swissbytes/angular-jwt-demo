/**
 * Created by jorgeburgos on 12/2/15.
 */
(function() {
    'use strict';

    angular.module('app.layout', ['app.core'])
        .directive('toggleLeftSidebar', toggleLeftSidebar);

    function toggleLeftSidebar() {
        return {
            restrict: 'A',
            template: '<button ng-click="toggleLeft()" class="sidebar-toggle" id="toggle-left"><i class="fa fa-bars"></i></button>',
            controller: ['$scope', function($scope) {
                $scope.toggleLeft = function() {
                    ($(window).width() > 767) ? $('#main-wrapper').toggleClass('sidebar-mini'): $('#main-wrapper').toggleClass('sidebar-opened');
                }
            }]
        };
    }
})();