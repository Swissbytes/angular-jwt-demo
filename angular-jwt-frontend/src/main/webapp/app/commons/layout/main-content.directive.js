/**
 * Created by jorgeburgos on 12/14/15.
 */

(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('mainContent', mainContent);

    mainContent.$inject = ['$state'];

    function mainContent($state) {
        var directive = {
            bindToController: true,
            controller: MainContentController,
            controllerAs: 'vm',
            restrict: 'EA',
            replace: true,
            scope: {},

            templateUrl: 'app/commons/layout/main-content.html'

        };

        function MainContentController() {
            var vm = this;
            vm.state = $state;
        }

        return directive;

    }

})();