/**
 * Created by jorgeburgos on 12/14/15.
 */

(function() {

    'use strict';

    angular
        .module('app')
        .controller('ConfirmationModalController', ConfirmationModalController);

    ConfirmationModalController.$inject = ['$state', '$uibModalInstance', 'properties'];
    /* @ngInject */
    /**
     * @return {string}
     */
    function ConfirmationModalController($state, $uibModalInstance, properties) {
        var pm = this;
        pm.title = properties.title;
        pm.okLabel = properties.okLabel;
        pm.cancelLabel = properties.cancelLabel;
        pm.comment = properties.comment;
        pm.message = properties.message;
        pm.ok = function() {$uibModalInstance.close(pm.comment);};
        pm.cancel = $uibModalInstance.dismiss;
    }
})();