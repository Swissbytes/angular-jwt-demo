(function () {

    'use strict';

    angular
        .module('app.layout')
        .directive('headerPage', headerPage);

    headerPage.$inject = ['dataservice'];

    function headerPage(dataservice) {
        var directive = {
            bindToController: true,
            controller: HeaderPageController,
            controllerAs: 'vm',
            restrict: 'E',
            replace: true,
            scope: {},

            templateUrl: 'app/commons/layout/header-page.html'
        };

        function HeaderPageController() {
            var vm = this;
            vm.sideVisible = false;
            vm.datasource = dataservice;
            vm.logout = logout;

            vm.toggleVisible = function () {
                vm.sideVisible = !vm.sideVisible;
            };

            function logout() {
                dataservice.logout();
                window.location.reload();
            }
        }

        return directive;
    }


})();