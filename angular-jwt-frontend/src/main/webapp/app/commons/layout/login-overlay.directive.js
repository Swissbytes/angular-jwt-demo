(function () {

    'use strict';

    angular
        .module('app.layout')
        .directive('loginOverlay', loginOverlay);

    loginOverlay.$inject = ['dataservice','logger','$state'];

    function loginOverlay(dataservice,logger,$state) {

        function loginOverlayController() {
            var vm = this;

            vm.loginState = true;
            vm.user = '';
            vm.pass = '';

            vm.login = function(){
                dataservice.getAuth(vm.user,vm.pass).then(function (data) {
                    if (data.status == 200) {
                        if (data.data) {
                            dataservice.setUserdata(data.data);
                            vm.loginState=false;

                            $state.go('solicitud.list.collaborators');
                        }
                    } else {
                        vm.loginState = true;
                        logger.warning("Error en usuario o contraseña");
                    }
                });
            };

            function init() {
                vm.loginState = true;
                vm.user = '';
                vm.pass = '';
                if (!!dataservice.isUserLoaded()) {
                    vm.loginState = false;
                } else {
                    $state.go('solicitud.list.collaborators');
                }
            }

            init();
        }

        var directive = {
            bindToController: true,
            controller: loginOverlayController,
            controllerAs: 'vm',
            restrict: 'EA',
            replace: true,
            scope: {},

            templateUrl: 'app/commons/layout/login-overlay.html'
        };

        return directive;
    }
})();