/**
 * Created by jorgeburgos on 12/14/15.
 * Modified by miguel on 1/4/16
 */

(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('sidebarLeft', sidebarLeft);

        sidebarLeft.$inject = ['dataservice', '$state'];

        function sidebarLeft(dataservice, $state) {
            var SidebarLeftController = function() {
                var vm = this;
                vm.datasource = dataservice;

                vm.logout = logout;
                vm.$state = $state;

                function logout() {
                    dataservice.logout();
                    window.location.reload();
                }

                //function inState(value) {
                //    return $state.includes(value);
                //}

                function navToggleSub() {
                    $(function() {
                        var subMenu = $('.sidebar .nav');
                        $(subMenu).navgoco({
                            caretHtml: false,
                            accordion: true
                        });
                    });
                }

                navToggleSub();
            };

            var directive = {
                bindToController: true,
                controller: SidebarLeftController,
                controllerAs: 'vm',
                restrict: 'E',
                replace: true,
                templateUrl: 'app/commons/layout/sidebar-left.html',
                scope: {}
            };

            return directive;
        }
})();