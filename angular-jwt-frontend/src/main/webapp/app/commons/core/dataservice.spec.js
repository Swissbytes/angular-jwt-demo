/**
 * Created by jorgeburgos on 12/15/15.
 */

describe('dataservice', function () {

    var $httpBackend;
    var dataservice;

    beforeEach(module('app.core'));

    beforeEach(inject(function (_dataservice_, _$httpBackend_) {
        dataservice = _dataservice_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('shoud be registered', function () {
        expect(dataservice).toBeDefined();
    });

    it('getUsers function', function () {
        it('should exist', function () {
            expect(dataservice.getUsers).toBeDefined();
        });

        it('should return 1 users', function () {
            $httpBackend
                .whenGET('/security/users')
                .response(200, [
                    {
                        id: 10,
                        name: "jorge",
                        lastName: "Burgos Lopez"
                    }
                ]);

            dataservice.getUsers().then(function (data) {
                expect(data.length).toEqual(5);
            });

            $httpBackend.flush();
        });

    });

});