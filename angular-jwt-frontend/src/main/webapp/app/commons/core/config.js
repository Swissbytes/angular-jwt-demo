/**
 * Created by jorgeburgos on 12/3/15.
 */

(function() {
    'use strict';

    var core = angular.module('app.core');

    core.config(['$httpProvider', function($httpProvider) {
        delete $httpProvider.defaults.headers.common["X-Requested-With"]
    }]);
    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[Application Error]', //configure the exceptionHandler decorator
        appTitle: 'YPFB Refinación',
        imageBasePath: 'images/',
        unknownPersonImageSource: 'unknown_person.jpg'
    };

    core.constant('config', config);

    core.config(configFn);

    configFn.$inject = ['config', 'environment','$httpProvider'];
    /* @ngInject */
    function configFn(config, environment,$httpProvider) {
        //config.env = environment;
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
        $httpProvider.defaults.headers.common['If-Modified-Since'] = '0';
    }


    core.config(configure);

    configure.$inject = ['$compileProvider', '$logProvider',
        'routerHelperProvider', 'exceptionHandlerProvider'];

    function configure($compilerProvider, $logProvider, routerHelperProvider, exceptionHandlerProvider) {
        $compilerProvider.debugInfoEnabled(false);

        //turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }

        exceptionHandlerProvider.configure(config.appErrorPrefix);
        configureStateHelper();

        ////////////////

        function configureStateHelper() {
            var resolveAlways = {
                ready: ready
            };

            ready.$inject = ['dataservice'];
            /* @ngInject */
            function ready(dataservice) {
                return dataservice.ready();
            }

            routerHelperProvider.configure({
                docTitle: 'YPFB Andina',
                resolveAlways: resolveAlways
            });
        }
    }

})();

