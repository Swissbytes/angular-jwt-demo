/**
 * Created by ramiro.crespo on 28/01/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .service('userservice', [ '$cookies', '$window',
            function ($cookies, $window) {

                var userdata = {
                    user: ''
                };

                var service = {
                    logout: logout,

                    setUserdata: setUserdata,
                    getUserdata: getUserdata,
                    getUser: getUser,
                    getKey: getKey,
                    getAccount: getAccount,
                    isUserLoaded: isUserLoaded
                };

                return service;

                function logout() {
                    userdata.key = null;
                    userdata.user = '';
                    $cookies.remove('userdata');
                }

                function setUserdata(data) {
                    userdata.user = data.user;
                    userdata.key = data.authorization;

                    var expire = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                    $cookies.put('userdata', JSON.stringify(userdata),{'expires': expire});
                }

                function getUserdata() {
                    if (!userdata.key) {
                        var cookiedata = $cookies.get('userdata')
                        userdata = !!cookiedata ? JSON.parse(cookiedata) : {};
                    }
                    return userdata;
                }

                function getUser() {
                    return getUserdata().user;
                }

                function getKey() {
                    return getUserdata().key;
                }

                function isUserLoaded() {
                    try {
                        return getUserdata().key != null && getUserdata().key.length > 0;
                    } catch(e) {
                        return false;
                    }
                }

                function parseJwt(token) {
                    var base64Url = token.split('.')[1];
                    var base64 = base64Url.replace('-', '+').replace('_', '/');
                    return JSON.parse($window.atob(base64));
                }

                function getAccount() {
                    return !!getKey() ? parseJwt(getKey()).account : {};
                }
            }]);
})();
