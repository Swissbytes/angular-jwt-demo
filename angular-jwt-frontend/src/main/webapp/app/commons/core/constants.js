/**
/**
 * Created by jorgeburgos on 12/2/15.
 */
(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('toastr', toastr);

})();