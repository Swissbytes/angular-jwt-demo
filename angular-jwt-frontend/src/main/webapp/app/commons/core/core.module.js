/**
 * Created by jorgeburgos on 12/2/15.
 */

(function () {
    'use strict';

    angular
        .module('app.core', [
            /* Angular modules */

            /* Cross-app modules */
            'blocks.exception',
            'blocks.logger',
            'blocks.router',
            'blocks.environment',
            /* 3rd-party modules */
            'ui.router'

        ]);
})();