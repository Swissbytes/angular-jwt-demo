(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$q', 'environment', 'exception', 'userservice'];
    /* @ngInject */
    function dataservice($http, $q, environment, exception, userservice) {

        var readyPromise;

        var service = {
            ready: ready,
            logout: logout,
            getAuth: getAuthorization,
            setUserdata: setUserdata,
            getUserdata: getUserdata,
            getAccount: getAccount,
            isUserLoaded: isUserLoaded,

            getSolicitudes: getSolicitudes,
            getSolicitud: getSolicitud,
            postSolicitudResponse: postSolicitudResponse
        };

        return service;

        function genericExceptionHandler(err) {
            if (!!err.data.msg) {
                exception.catcher(err.data.msg)(err);
            } else {
                exception.catcher(err.data.errorDescription)(err);
            }
        }

        function composeParams() {
            var len;
            var params;
            var i;
            for (i = 0, len = arguments.length, params = "?"; i < len; i++) {
                params += arguments[i];
                if (i < arguments.length - 1) {
                    params += "&";
                }
            }
            return params;
        }

        function genericGet(section, suffix) {
            //if (suffix === undefined) {
            //    suffix = "";
            //}
            var req = {
                method: 'GET',
                url: environment.baseRestfulPath + '/' + section + '/' + (suffix?suffix:''),
                headers: {
                    'Content-Type': "application/json",
                    'authorization': userservice.getKey()
                },

                data: {}
            };
            return $http(req).then(genericGetComplete)
                .catch(genericExceptionHandler);

            function genericGetComplete(data) {
                return data;
            }
        }

        function genericPost(section, suffix, data) {
            var req = {
                method: 'POST',
                url: environment.baseRestfulPath + '/' + section + '/' + (suffix?suffix:''),
                headers: {
                    'Content-Type': "application/json",
                    'authorization': userservice.getKey()
                },
                data: data
            };
            return $http(req).then(requestComplete)
                .catch(genericExceptionHandler);

            function requestComplete(data) {
                return data;
            }
        }

        function logout() {
            userservice.logout();
        }

        function getAuthorization(user, pass) {
            var req = {
                method: 'POST',
                url: environment.baseRestfulPath + '/users/login/' + user + '/' + pass,
                headers: {
                    'Content-Type': "application/json"
                },
                data: {}
            };
            return $http(req).then(requestComplete)
                .catch(genericExceptionHandler);

            function requestComplete(data) {
                return data;
            }
        }

        function getSolicitudes(type, userId, paramCode, paramOrderby, paging) {
            var req = 'solicitudes' + '/' + type + '/' + userId;
            var filters = composeParams(paramCode, paramOrderby, paging);
            return genericGet(req, filters);
        }

        function getSolicitud(solicitudId) {
            return genericGet('solicitudes', solicitudId);
        }

        function postSolicitudResponse(data) {
            return genericPost('solicitudes', null, data);
        }

        function setUserdata(data) {
            userservice.setUserdata(data);
        }

        function getUserdata() {
            return userservice.getUserdata();
        }

        function getAccount() {
            return userservice.getAccount();
        }

        function getReady() {
            if (!readyPromise) {
                readyPromise = $q.when(service);
            }
            return readyPromise;
        }

        function ready(promisesArray) {
            return getReady()
                .then(function () {
                    return promisesArray ? $q.all(promisesArray) : readyPromise;
                })
                .catch(exception.catcher('"ready" function failed'));
        }

        function isUserLoaded() {
            return userservice.isUserLoaded();
        }
    }
})();
