/**
 * Created by jorgeburgos on 12/2/15.
 */

var gulp = require('gulp');
var args = require('yargs').argv;
var del = require('del');
var runSequence = require('run-sequence');
var $ = require('gulp-load-plugins')({lazy: true});

var config = require('./gulp.config.js')();

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('vet', function () {
    log('Analyzing source with JSHint');

    return gulp
        .src(config.allSrcJs)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'), {verbose: true})
        .pipe($.jshint.reporter('fail'));

});

gulp.task('default', function () {
    if (args.prod || args.test) {
        return runSequence('optimize');
    } else {
        return runSequence('inject');
    }
});

gulp.task('optimize', function () {
    runSequence(
        ['inject', 'templatecache', 'clean-optimize'],
        ['fonts', 'images', 'web-inf'],
        'generateOptimizeFiles'
    );
})

gulp.task('generateOptimizeFiles', function () {
    log('Optimizing the javascript, css and html');

    var templateCache = config.webappTemp + config.templateCache.file;

    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, {read: false}), {
            starttag: '<!-- inject:templates:js -->',
            ignorePath: config.webappIgnore,
            addRootSlash: false
        }))
        .pipe($.useref({searchPath: './src/main/webapp/'}))
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.csso()))
        .pipe($.if('*.js', $.rev()))
        .pipe($.if('*.css', $.rev()))
        .pipe($.revReplace())
        .pipe(gulp.dest(config.optimizedDest));
});

gulp.task('inject', ['wiredep', 'less-styles'], function () {
    log('Wire up the app less into the html, and call wiredep');

    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.styles.css.src, {read: false}),
            {
                ignorePath: config.webappIgnore,
                addRootSlash: false
            }
        ))
        .pipe(gulp.dest(config.webapp))
});

gulp.task('wiredep', ['create-index', 'create-env-module'], function () {
    log('Wire up the bower css, js and our app js into html');

    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;

    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js, {read: false}),
            {
                ignorePath: config.webappIgnore,
                addRootSlash: false,
                addPrefix: config.webContext
            }
        ))

        .pipe(gulp.dest(config.webapp));
});

gulp.task('create-index', function () {
    log("Copy index.template.html to index.html");
    return copyIfNotExists(config.index, config.indexTemplate, "index.html", config.webapp);
});


gulp.task('less-styles', ['clean-less-styles'], function () {
    log('Compiling less ---> css, to ' + config.styles.less.destDir);

    return gulp
        .src(config.styles.less.mainFile)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.autoprefixer({browser: ['last 2 version', '> 3%']}))
        .pipe($.rename(config.styles.less.filenameCompressed))
        .pipe(gulp.dest(config.styles.less.destDir));

});

gulp.task('images', function () {
    return gulp
        .src(config.images.src)
        .pipe(gulp.dest(config.images.dest));
});

gulp.task('fonts', function () {
    return gulp
        .src(config.fonts.src)
        .pipe(gulp.dest(config.fonts.dest));
});

gulp.task('web-inf', function () {
    return gulp
        .src('src/main/webapp/WEB-INF/**/*')
        .pipe(gulp.dest('src/main/webapp/.optimized/WEB-INF'));
});

gulp.task('clean-less-styles', function () {
    var files = config.temp + config.styles.less.filenameCompressed;
    return clean(files);
});

gulp.task('clean-env', function () {
    var files = config.env.directory + config.env.filename;
    return clean(files);
});

gulp.task('clean-optimize', function () {
    var optimizeFolder = config.optimizedDest;
    return clean(optimizeFolder);
});

gulp.task('clean-templatecache', function () {
    var files = config.webappTemp + config.env.filename;
    return clean(files);
});


gulp.task('test', ['templatecache'], function (done) {
    startTests(true /* singleRun */, done);
});

////////////// Environment depend tasks
function startTests(singleRun, done) {
    var karma = require('karma').server;
    var excludeFiles = [];
    var serverSpecs = config.serverIntegrationSpecs;

    excludeFiles = serverSpecs;

    karma.start({
        configFile: __dirname + '/karma.conf.js',
        exclude: excludeFiles,
        singleRun: !!singleRun
    }, karmaCompleted);

    function karmaCompleted(karmaResult) {
        log('karma completed');

        if (karmaResult === 1) {
            done('Karma: test failed with code ' + karmaResult);
        } else {
            done();
        }
    }
}


gulp.task('create-local-json', function () {
    log("Copy dev.json to local.json");
    return copyIfNotExists(config.env.localJson, config.env.devJson, "local.json", config.env.directory);
});


gulp.task('create-env-module', ['clean-env', 'create-local-json'], function () {

    // default dev environment
    var files = [config.env.devJson, config.env.localJson];

    if (args.test) {
        files = [config.env.testJson]
    } else if (args.prod) {
        files = [config.env.prodJson]
    }

    return gulp
        .src(files)
        .pipe($.mergeJson('merged.json'))
        .pipe($.ngConfig(config.env.module, {createModule: false}))
        .pipe($.rename(config.env.filename))
        .pipe(gulp.dest(config.env.directory))
});


gulp.task('templatecache', ['clean-templatecache'], function () {
    log('Creating AngularJS $templatecache');

    return gulp
        .src(config.htmltemplates)
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
        ))
        .pipe(gulp.dest(config.webappTemp));
});

//////////////

function copyIfNotExists(filePathToCheck, filePathToCopy, filename, dest) {
    log("copyIfNotExists filePathToCopy:" + filePathToCheck + ", filePathToCopy: " + filePathToCopy +
    ", filename: " + filename + ", dest: " + dest);

    var fileExists = require('file-exists');
    if (!fileExists(filePathToCheck)) {
        return gulp
            .src(filePathToCopy)
            .pipe($.rename(filename))
            .pipe(gulp.dest(dest))
            ;
    }
}

function clean(path) {
    log('Cleaning: ' + $.util.colors.blue(path));
    return del(path);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}